package com.abrusdev.test.data.di

import android.app.Application
import com.abrusdev.test.data.remote.ServiceGenerator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    fun provideServiceGenerator(
        app: Application,
    ) = ServiceGenerator(app)


}