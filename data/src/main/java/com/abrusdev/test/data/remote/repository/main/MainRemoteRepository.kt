package com.abrusdev.test.data.remote.repository.main

import com.abrusdev.test.data.remote.ServiceGenerator
import com.abrusdev.test.data.remote.repository.base.BaseRemoteRepository
import com.abrusdev.test.data.remote.service.MainService
import com.abrusdev.test.data.utils.NetworkConnectivity
import com.abrusdev.test.domain.model.Resource
import com.abrusdev.test.domain.model.response.base.BaseListResponse
import com.abrusdev.test.domain.model.response.main.MainResponse
import javax.inject.Inject

class MainRemoteRepository @Inject constructor(
    private val serviceGenerator: ServiceGenerator,
    networkConnectivity: NetworkConnectivity
) : BaseRemoteRepository(networkConnectivity) {

    suspend fun getMain(): Resource<List<MainResponse>> {
        val service = serviceGenerator.createService(MainService::class.java)

        val response = processCall { service.getMain() }

        return when {
            response is BaseListResponse<*> -> {
                val list = response.items as List<*>
                Resource.Success(list.filterIsInstance<MainResponse>())
            }

            else -> {
                processError(response)
            }
        }
    }
}