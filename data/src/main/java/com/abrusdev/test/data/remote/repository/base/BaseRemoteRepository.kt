package com.abrusdev.test.data.remote.repository.base

import android.util.Log
import com.abrusdev.test.data.utils.NetworkConnectivity
import com.abrusdev.test.domain.Constants
import com.abrusdev.test.domain.model.Resource
import com.abrusdev.test.domain.model.response.base.BaseErrorResponse
import com.google.gson.Gson
import retrofit2.Response
import java.io.IOException

abstract class BaseRemoteRepository(
    private val networkConnectivity: NetworkConnectivity,
) {

    protected suspend fun processCall(responseCall: suspend () -> Response<*>): Any? {
        if (!networkConnectivity.isConnected()) {

            networkConnectivity.updateNetworkConnectivity(false)
            return Constants.ERROR_NO_CONNECTION
        }

        networkConnectivity.updateNetworkConnectivity(true)

        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()

            if (response.isSuccessful) {
                response.body()
            } else {
                val errorBody = response.errorBody()?.string()
                try {
                    val errorData = Gson().fromJson(errorBody, BaseErrorResponse::class.java)
                    errorData.code = responseCode
                    return errorData
                } catch (e: Exception) {
                    Log.e("RRR", "processCall: $e")
                }

                responseCode
            }
        } catch (e: IOException) {
            Log.e("RRR", "processCall: ${e}")
            Constants.ERROR_IO
        } catch (e: Exception) {
            Log.e("RRR", "processCall: ${e}")
            Constants.ERROR_UNKNOWN
        }
    }

    protected fun <T> processError(response: Any?): Resource<T> {
        return when (response) {
            is BaseErrorResponse -> {
                Resource.Error(response)
            }

            is Response<*> -> {
                Resource.Error(BaseErrorResponse(response.code(), "remote"))
            }

            is Int -> {
                Resource.Error(BaseErrorResponse(response as Int, "local"))
            }

            else -> {
                Resource.Error(BaseErrorResponse(Constants.ERROR_UNKNOWN, "local"))
            }
        }
    }
}