package com.abrusdev.test.data.remote

import android.app.Application
import com.abrusdev.test.domain.Constants
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ServiceGenerator(
    private val app: Application
) {

    private val okHttpBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

    private lateinit var retrofit: Retrofit

    private fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
//            level = if (BuildConfig.DEBUG)
//                HttpLoggingInterceptor.Level.BODY
//            else
//                HttpLoggingInterceptor.Level.NONE

            level = HttpLoggingInterceptor.Level.NONE
        }

        with(okHttpBuilder) {
            addInterceptor(loggingInterceptor)
            cache(provideCache())
            writeTimeout(5, TimeUnit.MINUTES)
            readTimeout(5, TimeUnit.MINUTES)
            connectTimeout(5, TimeUnit.MINUTES)
        }

        return okHttpBuilder.build()
    }

    private fun provideCache(): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(app.cacheDir, cacheSize.toLong())
    }

    private fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    fun <S> createService(serviceClass: Class<S>): S {
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(provideGson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(Constants.BASE_API)
            .client(provideOkHttpClient())
            .build()

        return retrofit.create(serviceClass)
    }

}