package com.abrusdev.test.data.repository.main

import com.abrusdev.test.domain.model.Resource
import com.abrusdev.test.domain.model.response.main.MainResponse
import com.abrusdev.test.domain.usecase.GetMainUseCase
import com.abrusdev.test.data.remote.repository.main.MainRemoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MainRepository @Inject constructor(
    private val remoteRepository: MainRemoteRepository,
    private val ioDispatcher: CoroutineContext,
) : GetMainUseCase {

    override suspend fun getMain(): Flow<Resource<List<MainResponse>>> {
        return flow {
         emit(remoteRepository.getMain())
        }.flowOn(ioDispatcher)
    }
}