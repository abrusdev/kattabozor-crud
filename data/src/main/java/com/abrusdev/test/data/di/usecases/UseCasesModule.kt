package com.abrusdev.test.data.di.usecases

import com.abrusdev.test.domain.usecase.GetMainUseCase
import com.abrusdev.test.data.repository.main.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCasesModule {

    @Provides
    fun provideMainUseCase(repository: MainRepository): GetMainUseCase = repository

}