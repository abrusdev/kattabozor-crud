package com.abrusdev.test.data.remote.service

import com.abrusdev.test.domain.model.response.base.BaseListResponse
import com.abrusdev.test.domain.model.response.main.MainResponse
import retrofit2.Response
import retrofit2.http.GET

interface MainService {

    @GET("offers")
    suspend fun getMain(): Response<BaseListResponse<MainResponse>>

}