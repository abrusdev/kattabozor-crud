package com.abrusdev.test.domain.usecase

import com.abrusdev.test.domain.model.Resource
import com.abrusdev.test.domain.model.response.main.MainResponse
import kotlinx.coroutines.flow.Flow

interface GetMainUseCase {

    suspend fun getMain(): Flow<Resource<List<MainResponse>>>

}