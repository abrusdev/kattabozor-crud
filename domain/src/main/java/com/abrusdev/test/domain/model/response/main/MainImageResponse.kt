package com.abrusdev.test.domain.model.response.main

import com.google.gson.annotations.SerializedName

data class MainImageResponse(
    @SerializedName("width") val width: String,
    @SerializedName("height") val height: String,
    @SerializedName("url") val url: String?,
)