package com.abrusdev.test.domain.model.response.main

import com.google.gson.annotations.SerializedName

data class MainAttrResponse(
    @SerializedName("name") val name: String,
    @SerializedName("value") val value: String,
)