package com.abrusdev.test.domain.model.response.base

import com.google.gson.annotations.SerializedName

data class BaseErrorResponse(
    @SerializedName("code") var code: Int,
    @SerializedName("message") val message: String = ""
)