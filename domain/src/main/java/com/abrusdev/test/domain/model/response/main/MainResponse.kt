package com.abrusdev.test.domain.model.response.main

import com.google.gson.annotations.SerializedName

data class MainResponse(

    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("brand") var brand: String,
    @SerializedName("category") var category: String,
    @SerializedName("merchant") var merchant: String,
    @SerializedName("attributes") var attributes: ArrayList<MainAttrResponse> = arrayListOf(),
    @SerializedName("image") var image: MainImageResponse
)