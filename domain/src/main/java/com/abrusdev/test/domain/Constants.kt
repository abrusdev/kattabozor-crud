package com.abrusdev.test.domain

object Constants {

    const val BASE_API = "https://www.kattabozor.uz/hh/test/api/v1/"

    const val ERROR_NO_CONNECTION = -2
    const val ERROR_IO = -2
    const val ERROR_UNKNOWN = -3

}