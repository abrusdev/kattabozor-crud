package com.abrusdev.test.domain.model.response.base

import com.google.gson.annotations.SerializedName

class BaseListResponse<T>(
    @SerializedName("offers", alternate = ["items"]) val items: List<T>,
)
