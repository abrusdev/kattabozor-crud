package com.abrusdev.test.domain.model

import com.abrusdev.test.domain.model.response.base.BaseErrorResponse


sealed class Resource<T>(
    val data: T? = null,
    val error: BaseErrorResponse? = null
) {
    class Success<T>(data: T) : Resource<T>(data)

    class Loading<T>(data: T? = null) : Resource<T>(data)

    class Error<T>(data: BaseErrorResponse) : Resource<T>(null, data)

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Loading<T> -> "Loading"
            is Error -> "Error[exception=${error.toString()}]"
        }
    }
}