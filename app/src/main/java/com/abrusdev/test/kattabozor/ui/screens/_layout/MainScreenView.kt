package com.abrusdev.test.kattabozor.ui.screens._layout

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.abrusdev.test.kattabozor.ui.components.AppLoader
import com.abrusdev.test.kattabozor.ui.screens._components.MainItemView
import com.abrusdev.test.kattabozor.ui.screens.state.MainState

object MainScreenView {

    @Composable
    fun Default(state: MainState) {

        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(state.items) {
                MainItemView.Default(response = it)
            }
        }

        if (state.isLoading.value)
            AppLoader.Default()
    }
}