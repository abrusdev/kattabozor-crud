package com.abrusdev.test.kattabozor.ui.screens.state

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.abrusdev.test.domain.model.response.main.MainResponse

data class MainState(
    val isLoading: MutableState<Boolean> = mutableStateOf(false),
    val items: SnapshotStateList<MainResponse> = mutableStateListOf()
)