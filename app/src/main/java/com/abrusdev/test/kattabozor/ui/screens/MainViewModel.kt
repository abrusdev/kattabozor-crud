package com.abrusdev.test.kattabozor.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.abrusdev.test.domain.model.Resource
import com.abrusdev.test.domain.usecase.GetMainUseCase
import com.abrusdev.test.kattabozor.ui.screens.state.MainState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getMainUseCase: GetMainUseCase
) : ViewModel() {

    val state = MainState()

    fun fetch() {
        viewModelScope.launch {
            state.isLoading.value = true

            getMainUseCase.getMain().collect {
                state.items.clear()

                if (it is Resource.Success)
                    state.items.addAll(it.data!!)

                state.isLoading.value = false
            }
        }
    }
}