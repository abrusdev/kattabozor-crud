package com.abrusdev.test.kattabozor.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.abrusdev.test.kattabozor.utils.extensions.simpleClickable

object AppLoader {

    @Composable
    fun Default() {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .simpleClickable { },
            contentAlignment = Alignment.Center
        ) {
            val strokeWidth = 5.dp

            CircularProgressIndicator(
                modifier = Modifier,
                strokeWidth = strokeWidth
            )
        }
    }
}